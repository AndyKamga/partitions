## Sujet :

**1 - Écrivez une fonction « partition » qui prend un paramètre « liste » et un paramètre « taille » et retourne une liste de sous liste, où chaque sous liste a au maximum « taille » éléments.**

Exemples d'entrées et sorties : partition([1,2,3,4,5], 2) retourne: [ [1,2], [3,4], [5] ] partition([1,2,3,4,5], 3) retourne: [ [1,2,3], [4,5] ]

partition([1,2,3,4,5], 1) retourne: [ [1], [2], [3], [4], [5] ]



**2 - Écrivez des tests unitaires avec Junit4 pour vérifier votre implémentation.** 

Important : Partez du principe que votre implémentation fera partie d'une librairie utilisé par plein d'autres applications (en production) Livraison attendu : une archive contenant les sources du projet, et les instructions pour les utiliser.



## **Outils de travail :**

- Springboot (Java 8)
- IDE : IntelliJ 2019

## Tester le programme

1- Ouvrez le projet dans votre IDE (De préférence IntelliJ) et installer les dépendances dans le fichier "*pom.xml*".

2- Exécuté la classe "***PartitionApplicationTests***" (Voir capture ci-dessous).

![capture](img/capture.png)