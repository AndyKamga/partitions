package com.partition.partition;

import com.partition.partition.services.PartitionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import org.assertj.core.util.Lists;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PartitionApplicationTests {

    private PartitionService service = new PartitionService();

    @Test
    public void partition() {
        List<Integer> list = Lists.newArrayList(1, 2, 3, 4, 5);
        int taille = 3;
        final List partition = service.partition(list, taille);

        // TEST SUR LA DERNIERE SOUS PARTITION

        List customPartition = (List) partition.get(partition.size()-1);
        List targetPartition = Lists.newArrayList(4, 5);

        // On vérifie la taille de la sous partition sélectionnée.
        assertThat(partition.size(), equalTo(2));

        // On compare les deux partitions.
        assertThat(customPartition, equalTo(targetPartition));
    }
}
