package com.partition.partition.services;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PartitionService {

    public List partition(List<Integer> list, int taille) {
        /*
            On pourrait utiliser une boucle "For" mais cela complexifierait d'avantage le code
            Nous allons dont utiliser un Map. C'est plus simple et plus lisible.
         */
        Map<Integer, List<Integer>> listMapping = list.stream().collect(Collectors.groupingBy(p -> (p - 1) / taille));

        return new ArrayList<List<Integer>>(listMapping.values());
    }
}
